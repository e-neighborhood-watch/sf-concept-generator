# Science Fiction Concept Generator

Generates random names for science fiction concepts.

Some "real life" science fiction concepts:

* Dyson Sphere
* Stellar Engine
* Matrioshka Brain

module Main where

import Control.Monad.Random

uniformM :: (Foldable t, MonadRandom m) => t (m a) -> m a
uniformM = join . uniform

oneScientistName :: MonadRandom m => m String
oneScientistName = uniformM
  [ pure "Dijkstra"
  , pure "von Neumann"
  , pure "Einstein"
  , pure "Bohr"
  , pure "de Broglie"
  , pure "Schroedinger"
  , pure "Heisenberg"
  , pure "Planck"
  , pure "Dirac"
  , pure "Feynman"
  , pure "Born"
  , pure "Maxwell"
  , pure "Faraday"
  , pure "Church"
  , pure "Turing"
  , pure "Gamow"
  , pure "Bethe"
  , pure "Alpher"
  , pure "de Bruijn"
  , pure "Dyson"
  , pure "Oppenheimer"
  , pure "Roko"
  , pure "Klein"
  , pure "Moebius"
  , pure "Hopf"
  , pure "Hilbert"
  , pure "Goedel"
  , pure "Peano"
  , pure "Russel"
  , pure "Quine"
  , pure "Escher"
  , pure "Penrose"
  , pure "Hamilton"
  , pure "Minkowski"
  , pure "Mandelbrot"
  , pure "Lukasiewicz"
  , pure "Loef"
  , pure "Euler"
  , pure "Lorentz"
  , pure "Zermelo"
  , pure "Cantor"
  , pure "Grothendieck"
  , pure "Yoneda"
  , pure "Banach"
  , pure "Hausdorff"
  , pure "Tarski"
  , pure "Ackermann"
  , pure "Boltzmann"
  , pure "Brouwer"
  , pure "Zorn"
  , pure "Kleene"
  , pure "de Morgan"
  , pure "Riemann"
  , pure "Skolem"
  , liftM2 (++)
    ( uniform
      [ "Tar"
      , "Minkow"
      , "Haus"
      , "Acker"
      , "Boltz"
      , "Rie"
      ]
    )
    ( uniform
      [ "ski"
      , "dorff"
      , "mann"
      ]
    )
  ]

scientistName :: MonadRandom m => m String
scientistName = uniformM
  [ oneScientistName
  , uncurry (+-+) <$> liftM2 (,) oneScientistName oneScientistName `ensuring` (uncurry (/=))
  ]

scienceExtraKoolz :: MonadRandom m => m String
scienceExtraKoolz = uniform
  [ "gun"
  , "basilisk"
  , "hydra"
  , "chimera"
  ]

shapes :: MonadRandom m => m String
shapes = uniformM
  [ uniform [ "sphere" , "ball" ]
  , pure "cube"
  , pure "cone"
  , pure "prism"
  , liftM2 (++) sciencePrefix $ uniform [ "bola", "boloid" ]
  , pure "bottle"
  , pure "strip"
  , pure "torus"
  , pure "tesseract"
  , pure "plane"
  , pure "ring"
  , pure "tube"
  , pure "disk"
  , liftM2 (++)
    ( uniform
      [ "poly"
      , "zono"
      ]
    )
    ( uniform
      [ "hedron"
      , "tope"
      ]
    )
  , ors
  ]

ors :: MonadRandom m => m String
ors = liftM2 (++)
  ( uniformM
    [ pure "spin"
    , pure "tens"
    , pure "vect"
    , pure "vers"
    , pure "matr"
    , pure "magm"
    , pure "group"
    , pure "ellips"
    , pure "mon"
    , (++ "funct") <$> uniform [ "bi", "co", "endo", "multi", "pro" ]
    ]
  )
  ( uniform
    [ "or"
    , "oid"
    ]
  )

proofObject :: MonadRandom m => m String
proofObject = uniform
  [ "lemma"
  , "theorem"
  , "axiom"
  , "conjecture"
  , "postulate"
  , "hypothesis"
  , "corrolary"
  , "paradox"
  , "identity"
  , "equality"
  , "inequality"
  , "fallacy"
  , "rule"
  , "law"
  , "algorithm"
  , "problem"
  , "criterion"
  ]

proof :: MonadRandom m => m String
proof = liftM2 (+++)
  ( uniformM
    [ ("the" +++) <$> uniformM
      [ process
      , scientistName
      , scienceObject
      ]
    , liftM2 ($)
      ( uniformM
        [ pure id
        , flip (+++) <$> process
        ]
      )
      ( (++ "'s") <$> oneScientistName )
    ]
  )
  proofObject

theory :: MonadRandom m => m String
theory = liftM2 (+++)
  ( uniformM
    [ mundaneObject
    , ors
    , scienceStructure
    ]
  )
  ( pure "theory"
  )

mundaneObject :: MonadRandom m => m String
mundaneObject = uniform
  [ "cage"
  , "hook"
  , "anvil"
  , "spindle"
  , "wheel"
  , "cradle"
  , "net"
  , "bottle"
  , "string"
  , "knot"
  , "braid"
  , "loop"
  , "razor"
  ]

scienceObject :: MonadRandom m => m String
scienceObject = uniformM
  [ uniform [ "brain", "mind", "consciousness" ]
  , pure "garden"
  , pure "forest"
  , pure "automaton"
  , pure "ylem"
  , pure "well"
  , pure "magma"
  , pure "fibration"
  , uniform [ "bomb", "cannon" ]
  , uniform [ "engine", "thruster" ]
  , uniform [ "material" , "matter" ]
  , uniform [ "hive", "nest" ]
  , computerPart
  , mundaneObject
  , scienceStructure
  , shapes
  ]

computerPart :: MonadRandom m => m String
computerPart = uniformM
  [ pure "computer"
  , pure "processor"
  , liftM2 (++) sciencePrefix improvisedPart
  , improvisedPart
  ]
  where
    improvisedPart :: MonadRandom m => m String
    improvisedPart =
      uniformM
        [ liftM2 (++)
            ( uniform
              [ "con"
              , "in"
              , "trans"
              ]
            )
            ( uniform
              [ "ductor"
              , "ducer"
              , "former"
              ]
            )
        , uniform
          [ "resistor"
          , "transistor"
          , "varistor"
          , "reactor"
          ]
        ]

scienceStructure :: MonadRandom m => m String
scienceStructure = uniform
  [ "net"
  , "mesh"
  , "web"
  , "swarm"
  , "storm"
  , "network"
  , "map"
  , "matrix"
  , "field"
  , "bundle"
  , "structure"
  , "film"
  , "grid"
  , "cloud"
  , "envelope"
  ]

letter :: MonadRandom m => m String
letter = pure <$> uniformM
  [ uniform (['A' .. 'Z'] ++ ['a' .. 'z'])
  , uniform ['א' .. 'ת']
  , uniformM
    [ uniform ['α' .. 'ω']
    , uniform "ΓΔΘΛΞΠΣΦΨΩ" 
    ]
  ]

sciencePrefix :: MonadRandom m => m String
sciencePrefix = uniformM
  [ pure "meta"
  , pure "proto"
  , pure "hyper"
  , pure "quasi"
  , pure "geo"
  , uniform [ "semi" , "hemi" ]
  , pure "nano"
  , pure "yotta"
  , pure "para"
  , pure "perma"
  , pure "bio"
  , pure "eco"
  , pure "multi"
  , pure "xeno"
  ]

process :: MonadRandom m => m String
process = uniform
  [ "extraction"
  , "collection"
  , "manufacturing"
  , "engineering"
  , "automation"
  , "computation"
  , "thought"
  , "processing"
  , "capture"
  , "synthesis"
  , "analysis"
  , "induction"
  , "construction"
  , "quantification"
  , "simulation"
  , "propulsion"
  , "learning"
  , "evolution"
  , "transfer"
  , "transport"
  , "confinement"
  , "encoding"
  , "lensing"
  , "abstraction"
  , "scrubbing"
  , "counting"
  ]

drunkNumber :: (MonadRandom m, Integral a) => a -> m a
drunkNumber n = uniformM [ return n, drunkNumber (n+1), drunkNumber (n-1) ]

ensuring :: MonadRandom m => m a -> (a -> Bool) -> m a
ensuring gen predicate = do
  result <- gen
  if predicate result
    then return result
    else ensuring gen predicate

scienceAdjective :: MonadRandom m => m String
scienceAdjective = uniformM
  [ pure "trancendental"
  , pure "dark"
  , pure "alien"
  , pure "exotic"
  , pure "autonomous"
  , pure "neural"
  , pure "social"
  , pure "quantum"
  , pure "nuclear"
  , pure "existential"
  , pure "solar"
  , pure "abstract"
  , pure "seismic"
  , pure "ambient"
  , pure "paradoxical"
  , uniformM [ pure "dimensional", pure "n-dimensional", (++ "-dimensional") . show <$> drunkNumber 3 `ensuring` (/= 3) `ensuring` (>= 0) ]
  , uniformM
    [ pure "finite"
    , pure "transfinite"
    , pure "infinite"
    , (++ "finite") <$> sciencePrefix
    ]
  , uniform [ "stochastic", "procedural", "probabalistic" ]
  , uniform [ "rational", "real", "complex" ]
  , uniform [ "passive" , "active" , "aggressive" ]
  , uniform [ "synthetic", "artificial", "simulated" ]
  , uniform [ "strange", "charm" ]
  , ("inter" ++) <$> grandScale
  , grandScale
  , ("sub" ++) <$> minuteScale
  , minuteScale
  ]
  where
    grandScale :: MonadRandom m => m String
    grandScale = uniform [ "continental", "orbital", "stellar", "planitary", "galactic", "universal", "dimensional" ]
    minuteScale :: MonadRandom m => m String
    minuteScale = uniform [ "molecular", "atomic", "nuclear", "quantum" ]

compoundAdjective :: MonadRandom m => m String
compoundAdjective = liftM2 (+-+)
  ( uniform
    [ "high"
    , "low"
    , "higher"
    , "lower"
    ]
  )
  ( uniform
    [ "density"
    , "temperature"
    , "latency"
    , "speed"
    , "power"
    , "charge"
    , "entropy"
    , "dimensional"
    ]
  )

(+++) :: String -> String -> String
(+++) = (++) . (++ " ")

(+-+) :: String -> String -> String
(+-+) = (++) . (++ "-")

scifiConcept :: MonadRandom m => m String
scifiConcept =
  uniformM
    [ liftM2 (+++) scientistName $ uniformM
      [ scienceObject
      , process
      ]
    , liftM2 (+++) compoundAdjective
      ( uniformM
        [ process
        , computerPart
        ]
      )
    , liftM2 (+++) scienceAdjective
      ( uniformM
        [ scienceObject
        , process
        , liftM2 (++) sciencePrefix scienceStructure
        ]
      )
    , uniformM
      [ theory
      , proof
      ]
    ]

main :: IO ()
main = pure ()
